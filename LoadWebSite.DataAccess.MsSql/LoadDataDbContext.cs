﻿using LoadWebSite.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.DataAccess.MsSql
{
    public class LoadDataDbContext : DbContext
    {
        public string ConString { get; set; }

        public LoadDataDbContext()
        {
            ConString = "Server=localhost,1400; Database=LoadDataDb; User Id=sa; Password=ppp@222DP;";
        }

        public DbSet<HostServer> HostServers { get; set; }
        public DbSet<LottoNumber> LottoNumbers { get; set; }
        public DbSet<RequiredData> RequiredDatas { get; set; }
        public DbSet<UrlParameter> UrlParameters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(this.ConString);
        }

    }
}
