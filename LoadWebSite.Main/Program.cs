﻿using LoadWebSite.Core;
using System;
using System.Linq;

namespace LoadWebSite.Main
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DbRepository.LoadSeedData();

            var hostServerId = 1;
            var generateUrl = new GenerateUrls();
            var l = generateUrl.Get(hostServerId);

            foreach (var item in l)
            {
                Console.WriteLine(item);
            }

            var grabData = new GrabDataFromUrl();

            var data = grabData.Get(l, hostServerId);


            foreach (var item in data)
            {
                Console.WriteLine(item.Url);
            }

            var saveData = new SaveLottoNumbers();

            saveData.Set(data);

            Console.WriteLine("Data Saved");

            Console.ReadKey();
        }
    }
}
