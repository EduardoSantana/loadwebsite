﻿using LoadWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadWebSite.Core
{
    public class GenerateUrls
    {
        public List<string> Get(int hostServerId)
        {
            var hostServer = DbRepository.GetHostServer(hostServerId);

            var maxIndex = 0;
            var splitBy = "|";
            var retVal = new List<string>();

            foreach (var item in hostServer.UrlParameters.OrderBy(p => p.Order))
            {
                if (item.Value.Contains(splitBy))
                {
                    var array = item.Value.Split(splitBy);

                    if (array.Length > maxIndex)
                    {
                        maxIndex = array.Length;
                    }
                }
            }

            for (int i = 0; i < maxIndex; i++)
            {
                var oneLink = hostServer.Link;
                foreach (var item in hostServer.UrlParameters.OrderBy(p => p.Order))
                {
                    var dataKey = item.Key;
                    var dataValue = item.Value;

                    // work with key
                    if (item.Key.Contains(splitBy))
                    {
                        var array = item.Key.Split(splitBy);

                        if (array.Length >= (i + 1))
                        {
                            dataKey = array[i];
                        }
                        else
                        {
                            dataKey = array[array.Length - 1];
                        }
                    }

                    // work with value
                    if (item.Value.Contains(splitBy))
                    {
                        var array = item.Value.Split(splitBy);

                        if (array.Length >= (i + 1))
                        {
                            dataValue = array[i];
                        }
                        else
                        {
                            dataValue = array[array.Length - 1];
                        }
                    }

                    if (item.UrlParameterType == UrlParameterType.UrlPath)
                    {
                        oneLink += $"/{dataKey}";
                    }
                    if (item.UrlParameterType == UrlParameterType.QueryString)
                    {
                        var hasInterrrogation = oneLink.Contains("?");

                        oneLink += $"{(hasInterrrogation ? "&" : "?")}{dataKey}={dataValue}";
                    }

                }
                retVal.Add(oneLink);
            }
            
            return retVal;
        }
    }
}
