﻿using LoadWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadWebSite.Core
{
    public static class FakeDataRepository
    {
        public static void LoadData()
        {
            var urlParameters = new List<UrlParameter>()
            {
                new UrlParameter { Order = 1, Id = 1, Title = "1", HostServerId = 1, Key = "site", Value = "",  UrlParameterType = UrlParameterType.UrlPath },
                new UrlParameter { Order = 2, Id = 2, Title = "2", HostServerId = 1, Key = "winningNumberSearch", Value = "",  UrlParameterType = UrlParameterType.UrlPath },
                new UrlParameter { Order = 3, Id = 3, Title = "3", HostServerId = 1, Key = "searchTypeIn", Value = "range",  UrlParameterType = UrlParameterType.QueryString },
                new UrlParameter { Order = 4, Id = 4, Title = "4", HostServerId = 1, Key = "gameNameIn", Value = "LOTTO",  UrlParameterType = UrlParameterType.QueryString },
                new UrlParameter { Order = 5, Id = 5, Title = "5", HostServerId = 1, Key = "fromDateIn", Value = "01%2F01%2F2001|01%2F01%2F2002|01%2F01%2F2003|01%2F01%2F2004|01%2F01%2F2005",  UrlParameterType = UrlParameterType.QueryString },
                new UrlParameter { Order = 6, Id = 6, Title = "6", HostServerId = 1, Key = "toDateIn", Value = "12%2F31%2F2001|12%2F31%2F2002|12%2F31%2F2003|12%2F31%2F2004|12%2F31%2F2005",  UrlParameterType = UrlParameterType.QueryString },
                new UrlParameter { Order = 7, Id = 7, Title = "7", HostServerId = 1, Key = "submitForm", Value = "Submit",  UrlParameterType = UrlParameterType.QueryString },
            };

            var requiredDatas = new List<RequiredData>()
            {
                new RequiredData { Id = 1, RequiredDataType = RequiredDataType.PlainText, FilterBy = "#bodyContent > section.column2 > section > table > tbody > tr > td:nth-child(1) > a > div" },
                new RequiredData { Id = 2, RequiredDataType = RequiredDataType.PlainText, FilterBy = "#bodyContent > section.column2 > section > table > tbody > tr > td:nth-child(2) > a > div" },
            };

            var serverHosts = new List<HostServer>()
            {
                new HostServer
                {
                    UrlParameters = urlParameters,
                    RequiredDatas = requiredDatas,
                    Id = 1,
                    Link = "https://www.flalottery.com"
                }
            };

            if (HostServers == null)
            {
                HostServers = new List<HostServer>();
            }

            if (!HostServers.Any())
            {
                HostServers = serverHosts;
            }
          
        }

        public static List<HostServer> HostServers { get; set; }

        public static HostServer GetHostServer(int id)
        {
            return HostServers.FirstOrDefault(p => p.Id == id);
        }

    }
}
