﻿using LoadWebSite.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Core
{

    public class GetDataModel
    {
        public string Url { get; set; }
        public List<Data> Data { get; set; }
    }

    public class Data
    {
        public RequiredData RequiredData { get; set; }
        public List<string> Result { get; set; }
    }

}
