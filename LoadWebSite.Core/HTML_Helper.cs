﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace LoadWebSite.Core
{
    public static class HTML_Helper
    {
        public static string Get(string urlLink)
        {
            string retVal = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlLink);
            HttpWebResponse respose = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(respose.GetResponseStream());
            retVal = sr.ReadToEnd();
            return retVal;
        }
    }
}
