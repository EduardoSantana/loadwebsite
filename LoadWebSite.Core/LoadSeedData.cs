﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LoadWebSite.DataAccess.MsSql;
using LoadWebSite.Models;

namespace LoadWebSite.Core
{
    public class LoadSeedData
    {
        public LoadDataDbContext dbContext { get; set; }

        public LoadSeedData()
        {
            dbContext = new LoadDataDbContext();
        }

        public void InitialData()
        {
            LoadServerTest();
        }

        private void LoadServerTest()
        {
           
            var testingServer = new HostServer
            {
                Title = "Testing",
                Description = "Testing Description",
                Link = "https://www.flalottery.com"
            };

            if (dbContext.HostServers.FirstOrDefault(p => p.Link == "https://www.flalottery.com") == null)
            {
                var inserted = dbContext.HostServers.Add(testingServer).Entity;
                dbContext.SaveChanges();
                
                var urlParameters = new List<UrlParameter>()
                {
                    new UrlParameter { Order = 1, Title = "1", HostServerId = inserted.Id, Key = "site", Value = "",  UrlParameterType = UrlParameterType.UrlPath },
                    new UrlParameter { Order = 2, Title = "2", HostServerId = inserted.Id, Key = "winningNumberSearch", Value = "",  UrlParameterType = UrlParameterType.UrlPath },
                    new UrlParameter { Order = 3, Title = "3", HostServerId = inserted.Id, Key = "searchTypeIn", Value = "range",  UrlParameterType = UrlParameterType.QueryString },
                    new UrlParameter { Order = 4, Title = "4", HostServerId = inserted.Id, Key = "gameNameIn", Value = "LOTTO",  UrlParameterType = UrlParameterType.QueryString },
                    new UrlParameter { Order = 5, Title = "5", HostServerId = inserted.Id, Key = "fromDateIn", Value = GetDateFrom(),  UrlParameterType = UrlParameterType.QueryString },
                    new UrlParameter { Order = 6, Title = "6", HostServerId = inserted.Id, Key = "toDateIn", Value = GetDateTo(),  UrlParameterType = UrlParameterType.QueryString },
                    new UrlParameter { Order = 7, Title = "7", HostServerId = inserted.Id, Key = "submitForm", Value = "Submit",  UrlParameterType = UrlParameterType.QueryString },
                };

                dbContext.UrlParameters.AddRange(urlParameters);

                dbContext.SaveChanges();

                var requiredDatas = new List<RequiredData>()
                {
                    new RequiredData { HostServerId = inserted.Id, Title = RequiredDataNames.DateString, RequiredDataType = RequiredDataType.PlainText, FilterBy = "#bodyContent > section.column2 > section > table > tbody > tr > td:nth-child(1) > a > div" },
                    new RequiredData { HostServerId = inserted.Id, Title = RequiredDataNames.NumberString, RequiredDataType = RequiredDataType.PlainText, FilterBy = "#bodyContent > section.column2 > section > table > tbody > tr > td:nth-child(2) > a > div" },
                };

                dbContext.RequiredDatas.AddRange(requiredDatas);

                dbContext.SaveChanges();

            }

        }

        private static string GetDateFrom()
        {
            var constant = "01%2F01%2F";
            var retVal = "";

            for (int i = 1988; i < 2020; i++)
            {
                retVal += constant + i.ToString() + "|";
            }

            retVal = retVal.Remove(retVal.Length - 1);

            return retVal;
        }

        private static string GetDateTo()
        {
            var constant = "12%2F31%2F";
            var retVal = "";

            for (int i = 1988; i < 2020; i++)
            {
                retVal += constant + i.ToString() + "|";
            }

            retVal = retVal.Remove(retVal.Length - 1);

            return retVal;
        }
    }
}
