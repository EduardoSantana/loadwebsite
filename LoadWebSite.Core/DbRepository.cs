﻿using LoadWebSite.DataAccess.MsSql;
using LoadWebSite.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadWebSite.Core
{
    public static class DbRepository
    {
        public static LoadDataDbContext GetContext()
        {
            return new LoadDataDbContext();
        }

        public static HostServer GetHostServer(int id)
        {
            var retVal = GetContext()
                .HostServers
                .Include(p => p.RequiredDatas)
                .Include(p => p.UrlParameters)
                .FirstOrDefault(p => p.Id == id); 

            return retVal;
        }

        public static void LoadSeedData()
        {
            var seed = new LoadSeedData();
            seed.InitialData();
        }

        public static void Insert(LottoNumber lottoNumber)
        {
            var db = GetContext();
            if (!db.LottoNumbers.Any(p => p.DateString == lottoNumber.DateString))
            {
                db.LottoNumbers.Add(lottoNumber);
                db.SaveChanges();
            }
          
        }
    }
}
