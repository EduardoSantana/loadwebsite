﻿using CsQuery;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Core
{
    public class GrabDataFromUrl
    {
        public List<GetDataModel> Get(List<string> urls, int hostServerId)
        {
            var hostServer = DbRepository.GetHostServer(hostServerId);
            
            var retVal = new List<GetDataModel>();

            foreach (var url in urls)
            {

                if (UrlIsAlreadyDone(url))
                {
                    continue;
                }
                var item = new GetDataModel() { Url = url, Data = new List<Data>() };

                CQ objCQ = HTML_Helper.Get(url);
               
                foreach (var filter in hostServer.RequiredDatas)
                {
                    var itemData = new Data { RequiredData = filter, Result = new List<string>() };
                 
                    CQ datas = objCQ[filter.FilterBy];

                    foreach (var data in datas)
                    {
                        itemData.Result.Add(data.InnerText);
                    }

                    item.Data.Add(itemData);

                }

                retVal.Add(item);

            }

            return retVal;
        }

        public bool UrlIsAlreadyDone(string url)
        {
            return false;
        }
    }
}
