﻿using LoadWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoadWebSite.Core
{
    public class SaveLottoNumbers
    {
        public void Set(List<GetDataModel> dataModels)
        {
            foreach (var dataModel in dataModels)
            {
                int countRows = dataModel.Data.FirstOrDefault().Result.Count;
               
                foreach (var data in dataModel.Data)
                {
                    if (countRows != data.Result.Count)
                    {
                        throw new Exception("Error on size of the data!");
                    }
                }

                for (int i = 0; i < countRows; i++)
                {
                    var lottoNumber = new LottoNumber();
                    foreach (var data in dataModel.Data)
                    {
                        if (data.RequiredData.Title == RequiredDataNames.DateString)
                        {
                            lottoNumber.DateString = data.Result.ElementAt(i);
                        }
                        if (data.RequiredData.Title == RequiredDataNames.NumberString)
                        {
                            lottoNumber.NumberString = data.Result.ElementAt(i);
                        }
                    }
                    DbRepository.Insert(lottoNumber);
                }

            }

        }

    }

}
