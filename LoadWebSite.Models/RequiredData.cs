﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LoadWebSite.Models
{
    public class RequiredData : FullAudict
    {

        public string Title { get; set; }
       
        public RequiredDataType RequiredDataType { get; set; }
        public string FilterBy { get; set; }

        [Required]
        public int HostServerId { get; set; }

        [ForeignKey("HostServerId")]
        public HostServer HostServer { get; set; }
    }
}
