﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Models
{
    public class LottoNumber : FullAudict
    {
        public LottoNumber()
        {
            this.LottoNumberType = LottoNumberType.FloridaLotto;
        }
        public string DateString { get; set; }
        public string NumberString { get; set; }
        public LottoNumberType LottoNumberType { get; set; }
    }
}
