﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Models
{
    public enum UrlParameterType
    {
        UrlPath = 1,
        QueryString
    }
}
