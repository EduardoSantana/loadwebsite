﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LoadWebSite.Models
{
    public class UrlParameter : FullAudict
    {
        public string Title { get; set; }
        public int Order { get; set; }
        public UrlParameterType UrlParameterType { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        [Required]
        public int HostServerId { get; set; }

        [ForeignKey("HostServerId")]
        public HostServer HostServer { get; set; }

    }
}
