﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Models
{
    public class FullAudict
    {
        public FullAudict()
        {
            this.CreatedTime = DateTime.Now;
            this.IsActive = true;
        }
        public int Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool IsActive { get; set; }
    }
}
