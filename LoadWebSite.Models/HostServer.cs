﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadWebSite.Models
{
    public class HostServer : FullAudict
    {
        public HostServer()
        {
            this.UrlParameters = new HashSet<UrlParameter>();
            this.RequiredDatas = new HashSet<RequiredData>();
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public ICollection<UrlParameter> UrlParameters { get; set; }
        public ICollection<RequiredData> RequiredDatas { get; set; }
    }
}
